# Python Code for Idea 1 and *part* of Idea 2
## Idea 1

- DataMining_Yelp.ipynb - retrieves businesses and their reviews and generate a TFIDF matrix
- Flask_YelpAssignment_API.ipynb - Contains API methods for retrieving similar businesses and comparing
- myflask.py - Actual python file running on the server
- tfidf_matrix.pkl - Serialized file containing the tfidf matrix
- businesses_id.pkl and businesses_id_only.pkl - Serialized files containing business data.

To open the ipynb files please use Jupyter notebook - http://jupyter.org/

Running the (flask) server:

```
python myflask.py
```

## Idea 2

- Neo4j.ipynb - This file connects to a Neo4j server and has a list of queries that retrieve user communities, the businesses that they like and then recommend businesses that they positively reviewed.

This file can be executed in a Jupyter notebook, the file is configured to connect to a Neo4j server.  