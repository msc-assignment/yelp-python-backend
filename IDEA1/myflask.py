#!/usr/bin/env python
from flask import Flask, request
import json
from flask import jsonify

from sklearn.metrics.pairwise import cosine_similarity
import mysql.connector
import numpy as np
import pickle

with open('businesses_id.pkl', 'rb') as f:
    businesses_id = pickle.load(f)
with open('businesses_id_only.pkl', 'rb') as f:
    businesses_id_only = pickle.load(f)
with open('tfidf_matrix.pkl', 'rb') as f:
    tfidf_matrix = pickle.load(f)

#used to measure effectiveness of similarity
def getBusinesses(maximum = 10):
    cnx = mysql.connector.connect(user='root', password='password',host='198.211.125.71',database='yelp_min')
    #cnx = mysql.connector.connect(user='root', password='',host='127.0.0.1',database='yelp_db')
    cursor = cnx.cursor(buffered=True)
    allBusinesses = []
    correctClassification = 0
    count = 0
    for i in range(0,maximum):
        idtoselect = i
        idtoselectplus = i + 1
        first_b = cosine_similarity(tfidf_matrix[idtoselect:idtoselectplus], tfidf_matrix)
        
        second_largest = np.partition(first_b.flatten(), -2)[-2]
        second_largest_index = np.argwhere(first_b == second_largest)[0][1]

        third_largest = np.partition(first_b.flatten(), -3)[-3]
        third_largest_index = np.argwhere(first_b == third_largest)[0][1]

        query_get_businesses = "SELECT id, name, neighborhood, review_count, stars, is_open FROM business WHERE (business.id='" + businesses_id[idtoselect][0] + "') or (business.id='" + businesses_id[second_largest_index][0] + "') or (business.id='" + businesses_id[third_largest_index][0] + "')"
        cursor.execute(query_get_businesses)
        similar_businesses = {}
        
        #get data and categories for each business found
        for (business) in cursor:
            cat_cursor = cnx.cursor(buffered=True)
            query_get_categories = "SELECT category.category, business.name, business.id \
                                    from business, category \
                                    where business.id = category.business_id and business.id='" + business[0] + "'"
            cat_cursor.execute(query_get_categories)
            bus_categories = []
            for (categories) in cat_cursor:
                #remove obvious categories
                if (categories[0] != "Restaurants" and categories[0] != "Food"):
                    bus_categories.append(categories[0])
            similar_businesses[business[0]] = {'business':business, 'categories': bus_categories}
        
        
        correctCategories = np.sum(np.isin(similar_businesses[businesses_id[idtoselect][0]]['categories'], similar_businesses[businesses_id[second_largest_index][0]]['categories']))
        correctCategories2 = np.sum(np.isin(similar_businesses[businesses_id[idtoselect][0]]['categories'], similar_businesses[businesses_id[third_largest_index][0]]['categories']))
        
        correctCatPercentage = correctCategories/len(similar_businesses[businesses_id[idtoselect][0]]['categories'])
        correctCat2Percentage = correctCategories2/len(similar_businesses[businesses_id[idtoselect][0]]['categories'])
        data = {}
        data[similar_businesses[businesses_id[idtoselect][0]]['business'][1]] = {
         'id': similar_businesses[businesses_id[idtoselect][0]]['business'][0],
         'categories': similar_businesses[businesses_id[idtoselect][0]]['categories'],
         'similar': {
                 similar_businesses[businesses_id[second_largest_index][0]]['business'][1]: {
                     'id': similar_businesses[businesses_id[second_largest_index][0]]['business'][0],
                     'similarity': second_largest,
                     'categories': similar_businesses[businesses_id[second_largest_index][0]]['categories'],
                     'equal_categories': str(correctCategories),
                     'equal_categories_percentage': str(correctCatPercentage)
                 },
                 similar_businesses[businesses_id[third_largest_index][0]]['business'][1]: {
                     'id': similar_businesses[businesses_id[third_largest_index][0]]['business'][0],
                     'similarity': third_largest,
                     'categories': similar_businesses[businesses_id[third_largest_index][0]]['categories'],
                     'equal_categories': str(correctCategories2),
                     'equal_categories_percentage': str(correctCat2Percentage)
                 }
             }
        }
        
        if (correctCatPercentage >= 0.3) or (correctCat2Percentage >= 0.3):
            correctClassification = correctClassification + 1
        
        #overall count
        count = count + 1;
        
        allBusinesses.append(data)
    accuracy = (correctClassification/count)*100
    data = {}
    data['accuracy'] = str(accuracy)
    allBusinesses.append(data)
    cursor.close()
    cnx.close()
    return allBusinesses
    #return data

def getSimilarBusinessesFor(b_id):
    try:
        b_index = businesses_id_only.index(b_id)
    except ValueError:
        return("Business ID doesn't exist")

    #print(businesses_id[b_index][0])
    sim = cosine_similarity(tfidf_matrix[b_index:b_index+1], tfidf_matrix)
    
    second_largest = np.partition(sim.flatten(), -2)[-2]
    second_largest_index = np.argwhere(sim == second_largest)[0][1]
    
    third_largest = np.partition(sim.flatten(), -3)[-3]
    third_largest_index = np.argwhere(sim == third_largest)[0][1]
    
    forth_largest = np.partition(sim.flatten(), -4)[-4]
    forth_largest_index = np.argwhere(sim == forth_largest)[0][1]
    
    cnx = mysql.connector.connect(user='root', password='password',host='198.211.125.71',database='yelp_min')    
    cursor = cnx.cursor(buffered=True)
    
    query_get_businesses = "SELECT id, name, neighborhood, review_count, stars, is_open \
                            FROM business WHERE \
                            (business.id='" + businesses_id[b_index][0] + "') \
                            or (business.id='" + businesses_id[second_largest_index][0] + "') \
                            or (business.id='" + businesses_id[third_largest_index][0] + "') \
                            or (business.id='" +businesses_id[forth_largest_index][0] + "' )"
    
    cursor.execute(query_get_businesses)
    
    similar_businesses = {}
    for (business) in cursor:
        cat_cursor = cnx.cursor(buffered=True)
        query_get_categories = "SELECT category.category, business.name, business.id \
                                from business, category \
                                where business.id = category.business_id and business.id='" + business[0] + "'"
        cat_cursor.execute(query_get_categories)
        bus_categories = []
        for (categories) in cat_cursor:
            bus_categories.append(categories[0])
        similar_businesses[business[0]] = {'business':business, 'categories': bus_categories}

    data = {}
    data[similar_businesses[businesses_id[b_index][0]]['business'][1]] = {
     'id': similar_businesses[businesses_id[b_index][0]]['business'][0],
     'categories': similar_businesses[businesses_id[b_index][0]]['categories'],
     'similar': {
             similar_businesses[businesses_id[second_largest_index][0]]['business'][1]: {
                 'id': similar_businesses[businesses_id[second_largest_index][0]]['business'][0],
                 'similarity': second_largest,
                 'categories': similar_businesses[businesses_id[second_largest_index][0]]['categories']
             },
             similar_businesses[businesses_id[third_largest_index][0]]['business'][1]: {
                 'id': similar_businesses[businesses_id[third_largest_index][0]]['business'][0],
                 'similarity': third_largest,
                 'categories': similar_businesses[businesses_id[third_largest_index][0]]['categories']
             },
             similar_businesses[businesses_id[forth_largest_index][0]]['business'][1]: {
                 'id': similar_businesses[businesses_id[forth_largest_index][0]]['business'][0],
                 'similarity': forth_largest,
                 'categories': similar_businesses[businesses_id[forth_largest_index][0]]['categories']
             }
         }
     }
        
    cursor.close()
    cat_cursor.close()
    cnx.close()
    return data

def compareBusinesses(b_id1, b_id2):
    try:
        b_index1 = businesses_id_only.index(b_id1)
        b_index2 = businesses_id_only.index(b_id2)
    except ValueError:
        return("One or both Business IDs doesn't exist")
    
    sim = cosine_similarity(tfidf_matrix[b_index1:b_index1+1], tfidf_matrix[b_index2:b_index2+1])
    
    cnx = mysql.connector.connect(user='root', password='password',host='198.211.125.71',database='yelp_min')
    cursor = cnx.cursor(buffered=True)
    query_get_businesses = "SELECT id, name, neighborhood, review_count, stars, is_open \
                            FROM business WHERE \
                            (business.id='" + businesses_id[b_index1][0] + "') \
                            or (business.id='" + businesses_id[b_index2][0] + "')"
    
    cursor.execute(query_get_businesses)
    similar_businesses = {}
    for (business) in cursor:
        cat_cursor = cnx.cursor(buffered=True)
        query_get_categories = "SELECT category.category, business.name, business.id \
                                from business, category \
                                where business.id = category.business_id and business.id='" + business[0] + "'"
        cat_cursor.execute(query_get_categories)
        bus_categories = []
        for (categories) in cat_cursor:
            bus_categories.append(categories[0])
        similar_businesses[business[0]] = {'business':business, 'categories': bus_categories}

    data = {}
    data[similar_businesses[businesses_id[b_index1][0]]['business'][1]] = {
        'id': similar_businesses[businesses_id[b_index1][0]]['business'][0],
        'categories': similar_businesses[businesses_id[b_index1][0]]['categories']
    }
    data[similar_businesses[businesses_id[b_index2][0]]['business'][1]] = {
        'id': similar_businesses[businesses_id[b_index2][0]]['business'][0],
        'categories': similar_businesses[businesses_id[b_index2][0]]['categories'],
    }
    data['similarity'] = sim[0][0]
     
    cursor.close()
    cat_cursor.close()
    cnx.close()
    return data

app = Flask(__name__)

@app.route('/')
def hello_world():
    return "Hello World"

@app.route("/getData",methods=['GET'])
def _getData():
    businesses = 10
    
    if (request.args.get("businesses")):
        businesses = request.args.get("businesses")
        
    data = getBusinesses(int(businesses))
    return jsonify(data)

@app.route("/getSimilarBusiness",methods=['GET'])
def _getSimilarBusiness():
    if (request.args.get("b_id")):
        b_id = request.args.get("b_id")
        data = getSimilarBusinessesFor(b_id)
        return jsonify(data)
    else:
        return jsonify("No Business ID (b_id) param passed")
    
@app.route("/compareBusinesses",methods=['GET'])
def _compareBusinesses():
    if (request.args.get("b_id1") and request.args.get("b_id2")):
        b_id1 = request.args.get("b_id1")
        b_id2 = request.args.get("b_id2")
        data = compareBusinesses(b_id1, b_id2)
        return jsonify(data)
    else:
        return jsonify("Please pass b_id1 and b_id2 params")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)